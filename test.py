from knn import KNNClassifier
import numpy as np
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

# Read data from an file
def read_data(filepath,sep=','):
    dataset = []
    labels = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        for line in lines:
            if line.startswith('\n') == False:  # filter lines which starts with the symbol '\n'
                sepal_length,sepal_width,petal_length,petal_width,type = line.strip().split(sep)[:]
                dataset.append([float(sepal_length),float(sepal_width),float(petal_length),float(petal_width)])
                labels.append(type)
    return dataset, labels

if __name__ == '__main__':
    dataset,labels = read_data("data/iris.data")
    sample = [5.0,3.4,1.5,0.2]
    knnClassifier = KNNClassifier(3)
    knnClassifier.fit(dataset,labels)
    label = knnClassifier.predict(sample)
    print(label)