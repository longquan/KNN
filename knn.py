import numpy as np
import operator

class KNNClassifier():
    def __init__(self,k=3):
        self.k = k
    def fit(self,X,y):
        self.X = X if isinstance(X,np.ndarray) else np.asarray(X)
        self.y = y
    def predict(self,input):
        input if isinstance(input,np.ndarray) else np.asarray(input)
        datasetSize = self.X.shape[0]

        ''' calculate euclidean distances for similarity '''
        diffMat = np.tile(input,(datasetSize,1)) - self.X
        squaredDiffMat = diffMat ** 2
        squaredDistances = np.sum(squaredDiffMat,axis=1)
        distances = squaredDistances ** 0.5
        sortedDistIndicies = distances.argsort()

        classCount = {}
        for i in range(self.k):
            voteClass = self.y[sortedDistIndicies[i]]
            classCount[voteClass] = classCount.get(voteClass,0) + 1
        sortedClassCount = sorted(classCount.items(),key=operator.itemgetter(1),reverse=True)
        return sortedClassCount[0][0]