# KNN

## Description
This is a basic implementation of K-nearest Neighbor Classification Algorithm in Python.

## Dataset used
__iris.data__ is perhaps the best known database to be found in the pattern recognition literature.
For detailed info: [http://archive.ics.uci.edu/ml/datasets/Iris](http://archive.ics.uci.edu/ml/datasets/Iris)

## Usage
Look for code in ` test.py `

## Dependencies

 * Numpy
